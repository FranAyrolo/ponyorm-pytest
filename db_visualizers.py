from pony.orm import db_session
from db_schema import *

@db_session
def insert_client(name):
    c = db.Client(full_name=name)
    commit()

@db_session
def show_client_data(c_id):
    c = Client[c_id]
    print(c.full_name)

@db_session
def show_all_clients():
    query = select((c.client_ID, c.full_name) for c in Client)
    print(query[:])

