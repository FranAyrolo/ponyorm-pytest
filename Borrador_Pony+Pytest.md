# Objetivos:

- ## Mostrar el funcionamiento básico de PonyORM y la librería PyTest
- ## Ejemplificar buenas prácticas de programación usando frameworks/librerías
- ## Ejemplificar buenas prácticas de testing
- ## (Y ojalá, ver un *patrón de diseño*, en este caso Model View Controller)

# El modelo

Modelo simple, por ahora es:
Se encarga modelar el software de un banco que tiene empleados, clientes, cuentas y cajeros (sucursales, que agrupen empleados y cajeros? quizás, o de tarea)
El banco no es una clase en sí porque es la entidad top level en este caso
Todas las clases tienen un id autoincremental por simpleza
### Empleados tienen:
- Nombre completo
- Sueldo
- Token de autorización para transacciones

### Clientes tienen:
- Cuenta(s)
- Nombre Completo

### Cuentas tienen:
- número de cuenta
- balance

### Cajeros tienen:
- dinero disponible
- ubicación
- token de seguridad transacciones

#### El banco tiene una reserva de dinero no accesibles inmediatamente (como oro, inmuebles, etc... ?) y un balance líquido, accesible en todo momento

### Tareas Pony:
- Hacer un script que crea las clases de la base de datos
- Popular una base de datos básica 
- Implementar funcionalidad que permita manejar distintas bases de datos de un mismo modelo (conceptos para alumnos: separación de funcionalidades (separar la/s instancias de la BD del código gestor de la BD)
- Mostrar el uso de la estructura de archivos y separar funcionalidades en archivos. Usar archivos de configuración dev y prod (este año no hay tanta diferencia entre dev y prod porque no hay usuarios persistentes)
### Tareas Pytest
- Construir tests automatizados que vayan probando el proyecto bottom-up. Asegurar primero funcionalidades básicas como la creación y destrucción de la BD confiablemente. Luego asegurar la inserción correcta de elementos en la BD, luego asegurar actualización correcta de elementos, luego eliminación, etc.. etc...
- Demostrar la importancia de automatizar los tests de forma que corran siempre, por más que "hayan dejado de fallar" (!!!) (regression testing creo que era esto)
- (demostrar unit testing?)
- Cuando se hacen tests, qué información muestro en logs, consola, etc..? Diferenciar entre info de un test que pasa y uno que no

