from pony.orm import *

db = Database()
db.bind(provider='sqlite', filename=':sharedmemory:')

class Account(db.Entity):
    acc_ID = PrimaryKey(int, auto=True)
    acc_number = Required(int)
    balance = Required(int)

class Employee(db.Entity):
    employee_id = PrimaryKey(int, auto=True)
    full_name = Required(str)
    auth_token = Required(str)

class Client (db.Entity):
    client_ID = PrimaryKey(int, auto=True)
    full_name = Required(str)
    #accounts = Set(Account)

class ATM (db.Entity):
    ATM_ID = PrimaryKey(int, auto=True)
    available_balance = Required(int)	  
    location_address = Required(str) 
    auth_token = Required(str)

class BankReserves (db.Entity):
    money_in_reserves = Required(int)
    available_money = Required(int)

db.generate_mapping(create_tables=True)
